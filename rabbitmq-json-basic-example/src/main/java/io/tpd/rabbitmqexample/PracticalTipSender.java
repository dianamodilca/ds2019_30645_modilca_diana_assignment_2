package io.tpd.rabbitmqexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PracticalTipSender {

    private static final Logger log = LoggerFactory.getLogger(PracticalTipSender.class);
    private List<MonitoredData> data;
    int cont;
    private final RabbitTemplate rabbitTemplate;


    public PracticalTipSender(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        data = readFile();
    }

    public List<MonitoredData> readFile() {
        String fileName = "D:\\Facultă\\AN 4\\Sisteme Distribuite\\Laborator\\rabbitmq-json-basic-example\\src\\main\\resources\\activity.txt";
        List<MonitoredData> filedata = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(x -> {
                        MonitoredData mon = new MonitoredData();
                        String str1 = x.split("		")[0];
                        mon.setStartTime(str1);
                        String str2 = x.split("		")[1];
                        mon.setEndTime(str2);
                        mon.setActivity(x.split("		")[2]);
                        filedata.add(mon);
                    }
            );

        } catch (IOException e) {
            e.printStackTrace();
        }
        return filedata;
    }

    @Scheduled(fixedDelay = 3000L)
    public void sendData() {
        log.info("Send data, cont = " + cont);
        if (cont < data.size()) {
            rabbitTemplate.convertAndSend(RabbitmqExampleApplication.EXCHANGE_NAME, RabbitmqExampleApplication.ROUTING_KEY, data.get(cont));
            cont++;
        }
    }
//    public void sendPracticalTip() {
//        PracticalTipMessage tip = new PracticalTipMessage("Always use Immutable classes in Java", 1, false);
//        rabbitTemplate.convertAndSend(RabbitmqExampleApplication.EXCHANGE_NAME, RabbitmqExampleApplication.ROUTING_KEY, tip);
//        log.info("Practical Tip sent");
//    }
}
