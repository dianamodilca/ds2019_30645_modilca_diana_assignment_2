package com.sd.assignment1.controller;

//import com.sd.assignment1.dto.DoctorDTO;
//import com.sd.assignment1.dto.DoctorViewDTO;
import com.sd.assignment1.entities.Doctor;
import com.sd.assignment1.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    @Autowired
    private DoctorService doctorService;

    @GetMapping("/view")
    public List<Doctor> findAll(){
        return doctorService.findAll();
    }

    @GetMapping("/view/{id}")
    public Doctor findDoctorById(@PathVariable int id){
        return doctorService.findDoctorById(id);
    }
//
//    @GetMapping("/view/username/{username}")
//    public Doctor findDoctorByUsername(@PathVariable("username") String username){
//        return doctorService.findDoctorByUsername(username);
//    }

    @PostMapping("/insert")
    public Integer insertDoctorDTO(@RequestBody Doctor doctor) { return doctorService.insert(doctor);}

    @PutMapping("/update/{id}")
    public Integer updateDoctor(@RequestBody Doctor doctor, @PathVariable int id) {return doctorService.update(doctor, id);}

    @DeleteMapping("/delete/{id}")
    public void deleteDoctor( @PathVariable int id){
        doctorService.delete(id);
    }

}
