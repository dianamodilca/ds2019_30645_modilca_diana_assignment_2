package com.sd.assignment1.controller;

//import com.sd.assignment1.services.PatientService;
import com.sd.assignment1.entities.Patient;
import com.sd.assignment1.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {


    @Autowired
    private PatientService patientService;

    @GetMapping("/view")
    public List<Patient> findAll(){
        return patientService.findAll();
    }

    @GetMapping("/view/{id}")
    public Patient findPatientById(@PathVariable int id){
        return patientService.findPatientById(id);
    }

    @PostMapping("/insert")
    public Integer insertPatientDTO(@RequestBody Patient patient) { return patientService.insert(patient);}

    @PutMapping("/update/{id}")
    public Integer updatePatient(@RequestBody Patient patient, @PathVariable int id) {return patientService.update(patient, id);}

    @DeleteMapping("/delete/{id}")
    public void deletePatient( @PathVariable int id){
        patientService.delete(id);
    }

}
