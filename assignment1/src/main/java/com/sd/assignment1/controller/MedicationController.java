package com.sd.assignment1.controller;

import com.sd.assignment1.entities.Medication;
import com.sd.assignment1.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    @Autowired
    private MedicationService medicationService;


    @GetMapping("/view")
    public List<Medication> findAll(){
        return medicationService.findAll();
    }

    @GetMapping("/view/{id}")
    public Medication findMedicationById(@PathVariable int id){
        return medicationService.findMedicationById(id);
    }

    @PostMapping("/insert")
    public Integer insertMedicationDTO(@RequestBody Medication medication) { return medicationService.insert(medication);}

    @PutMapping("/update/{id}")
    public Integer updateMedication(@RequestBody Medication medication, @PathVariable int id) {return medicationService.update(medication, id);}

    @DeleteMapping("/delete/{id}")
    public void deleteMedication( @PathVariable int id){
        medicationService.delete(id);
    }
}
