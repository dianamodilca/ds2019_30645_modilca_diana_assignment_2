package com.sd.assignment1.controller;

import com.sd.assignment1.entities.Intake;
import com.sd.assignment1.services.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/intake")
public class IntakeController {

    @Autowired
    private IntakeService intakeService;

    @GetMapping("/view")
    public List<Intake> findAll(){
        return intakeService.findAll();
    }

    @GetMapping("/view/{id}")
    public Intake findIntakeById(@PathVariable int id){
        return intakeService.findIntakeById(id);
    }

    @PostMapping("/insert")
    public Integer insertIntakeDTO(@RequestBody Intake intake) { return intakeService.insert(intake);}

    @PutMapping("/update/{id}")
    public Integer updateIntake(@RequestBody Intake intake, @PathVariable int id) {return intakeService.update(intake, id);}

    @DeleteMapping("/delete/{id}")
    public void deleteIntake( @PathVariable int id){
        intakeService.delete(id);
    }

}
