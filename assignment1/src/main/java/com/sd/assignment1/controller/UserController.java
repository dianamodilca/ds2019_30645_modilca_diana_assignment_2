package com.sd.assignment1.controller;

import com.sd.assignment1.entities.Doctor;
import com.sd.assignment1.entities.User;
import com.sd.assignment1.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/username/{username}")
    public User findDoctorByUsername(@PathVariable("username") String username){
        return userService.findUserByUsername(username);
    }
}
