package com.sd.assignment1.controller;

import com.sd.assignment1.entities.Caregiver;
import com.sd.assignment1.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    @Autowired
    private CaregiverService caregiverService;

    @GetMapping("/view")
    public List<Caregiver> findAll(){
        return caregiverService.findAll();
    }

    @GetMapping("/view/{id}")
    public Caregiver findCaregiverById(@PathVariable int id){
        return caregiverService.findCaregiverById(id);
    }

    @PostMapping("/insert")
    public Integer insertCaregiverDTO(@RequestBody Caregiver caregiver) { return caregiverService.insert(caregiver);}

    @PutMapping("/update/{id}")
    public Integer updateCaregiver(@RequestBody Caregiver caregiver, @PathVariable int id) {return caregiverService.update(caregiver, id);}

    @DeleteMapping("/delete/{id}")
    public void deleteCaregiver( @PathVariable int id){
        caregiverService.delete(id);
    }

}
