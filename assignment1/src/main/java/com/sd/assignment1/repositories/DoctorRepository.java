package com.sd.assignment1.repositories;

import com.sd.assignment1.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

//    @Query(value = "SELECT u " +
//            "FROM Doctor u " +
//            "ORDER BY u.name")
//    List<Doctor> getAllOrdered();
//
//
//    @Query(value = "SELECT p " +
//            "FROM Doctor p " +
//            "INNER JOIN FETCH p.items i"
//    )
//    List<Doctor> getAllFetch();

    // Optional<Doctor> findById(Integer id);
}
