package com.sd.assignment1.services;

import com.sd.assignment1.entities.Intake;
import com.sd.assignment1.errorhandler.ResourceNotFoundException;
import com.sd.assignment1.repositories.IntakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IntakeService {

    @Autowired
    private IntakeRepository intakeRepository;

    public Intake findIntakeById(Integer id){
        Optional<Intake> intake  = intakeRepository.findById(id);

        if (!intake.isPresent()) {
            throw new ResourceNotFoundException("Intake", "user id", id);
        }
        Intake i = new Intake();

        i.setIdIntake(intake.get().getIdIntake());
        i.setStartDate(intake.get().getStartDate());
        i.setEndDate(intake.get().getEndDate());
        i.setPatient(intake.get().getPatient());
        i.setDoctor(intake.get().getDoctor());
        i.setMedicationList(intake.get().getMedicationList());

        return intakeRepository.save(i);
    }

    public List<Intake> findAll(){
        List<Intake> intakes = intakeRepository.findAll();

        return intakes;
    }


    public Integer insert(Intake intake) {


        Intake i = intakeRepository.save(intake);

        return i.getIdIntake();

    }

    public Integer update(Intake intake, int id) {

        Optional<Intake> intakee = intakeRepository.findById(id);
        // Intake d = new Intake();

        intake.setIdIntake(id);
        intakeRepository.save(intake);
//        d.setPassword(doc.get().getPassword());
//        d.setUsername(doc.get().getUsername());
//        d.setRole(doc.get().getRole());
//        d.setName(doc.get().getName());
//        d.setEmail(doc.get().getEmail());
//        d.setPatientList(doc.get().getPatientList());
//        d.setCaregiverList(doc.get().getCaregiverList());
//        d.setIntakeList(doc.get().getIntakeList());
        //     intakeRepository.save(d);

        return intake.getIdIntake();
    }

    public void delete(int id){
        this.intakeRepository.deleteById(id);
    }

}
