package com.sd.assignment1.services;

import com.sd.assignment1.entities.User;
import com.sd.assignment1.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User findUserByUsername(String username){
        List<User> users = userRepository.findAll();
        for(User user: users){
            if(user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }
    
    
}
