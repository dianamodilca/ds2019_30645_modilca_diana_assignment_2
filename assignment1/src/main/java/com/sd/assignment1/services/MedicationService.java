package com.sd.assignment1.services;

import com.sd.assignment1.entities.Medication;
import com.sd.assignment1.errorhandler.ResourceNotFoundException;
import com.sd.assignment1.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MedicationService {

    @Autowired
    private MedicationRepository medicationRepository;

    public Medication findMedicationById(Integer id){
        Optional<Medication> medication  = medicationRepository.findById(id);

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "user id", id);
        }
        Medication m = new Medication();
        m.setIdMedication(medication.get().getIdMedication());
        m.setName(medication.get().getName());
        m.setDosage(medication.get().getDosage());
        m.setPatient(medication.get().getPatient());
        m.setMedicationIntakeList(medication.get().getMedicationIntakeList());

        return medicationRepository.save(m);
    }

    public List<Medication> findAll(){
        List<Medication> medications = medicationRepository.findAll();

        return medications;
    }


    public Integer insert(Medication medication) {


        Medication m = medicationRepository.save(medication);

        return m.getIdMedication();

    }

    public Integer update(Medication medication, int id) {

        Optional<Medication> doc = medicationRepository.findById(id);
        // Medication d = new Medication();

        medication.setIdMedication(id);
        medicationRepository.save(medication);
//        d.setPassword(doc.get().getPassword());
//        d.setUsername(doc.get().getUsername());
//        d.setRole(doc.get().getRole());
//        d.setName(doc.get().getName());
//        d.setEmail(doc.get().getEmail());
//        d.setPatientList(doc.get().getMedicationServiceList());
//        d.setCaregiverList(doc.get().getCaregiverList());
//        d.setIntakeList(doc.get().getIntakeList());
        //     medicationServiceRepository.save(d);

        return medication.getIdMedication();
    }

    public void delete(int id){
        this.medicationRepository.deleteById(id);
    }

}