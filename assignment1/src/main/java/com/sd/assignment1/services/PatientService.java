package com.sd.assignment1.services;

//import com.sd.assignment1.dto.PatientDTO;
//import com.sd.assignment1.dto.builders.PatientBuilder;
import com.sd.assignment1.entities.Patient;

import com.sd.assignment1.errorhandler.ResourceNotFoundException;
import com.sd.assignment1.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {
    @Autowired
    private PatientRepository patientRepository;
//
//
//    public List<PatientViewDTO> findAll(){
//        List<Patient> patients = patientRepository.findAll();
//
//        return patients.stream()
//                .map(PatientViewBuilder::generateDTOFromEntity)
//                .collect(Collectors.toList());
//    }
//
////    public List<PersonWithItemsDTO> findAllFetch(){
////        List<Person> personList = personRepository.getAllFetch();
////
////        return personList.stream()
////                .map(x-> PersonWithItemsBuilder.generateDTOFromEntity(x, x.getItems()))
////                .collect(Collectors.toList());
////    }
//
//
//    //WRONG - without fetch an additional query is executed for each FK
////    public List<PersonWithItemsDTO> findAllFetchWrong(){
////        List<Person> personList = personRepository.findAll();
////
////        return personList.stream()
////                .map(x-> PersonWithItemsBuilder.generateDTOFromEntity(x, x.getItems()))
////                .collect(Collectors.toList());
////    }
//
//    @PostMapping()
//    public Integer insert(PatientDTO patientDTO) {
//
//        PAtientFieldValidator.validateInsertOrUpdate(patientDTO);
//
////        Patient patient = patientRepository.findById(patientDTO.getId());
//
//        return patientRepository
//                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
//                .getId();
//    }
//
//    public Integer update(PatientDTO patientDTO, Integer id) {
//
//        Optional<Patient> patient = patientRepository.findById(id);
//        if(!patient.isPresent()){
//            throw new ResourceNotFoundException("Patient", "user id", id.toString());
//        }
//
//        PatientFieldValidator.validateInsertOrUpdate(patientDTO);
//
//        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
//    }
//
//    public void delete(PatientViewDTO patientViewDTO){
//        this.patientRepository.deleteById(patientViewDTO.getId());
//    }

    public Patient findPatientById(Integer id){
        Optional<Patient> patient  = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", id);
        }
        Patient p = new Patient();
        p.setUserID(patient.get().getUserID());
        p.setPassword(patient.get().getPassword());
        p.setUsername(patient.get().getUsername());
        p.setRole(patient.get().getRole());
        p.setName(patient.get().getName());
        p.setBirthDate(patient.get().getBirthDate());
        p.setGender(patient.get().getGender());
        p.setAddress(patient.get().getAddress());
        p.setMedicalRecord(patient.get().getMedicalRecord());
        p.setIntakeList(patient.get().getIntakeList());
        p.setDoctor(patient.get().getDoctor());
       // p.setCaregiver(patient.get().getCaregiver());

        return patientRepository.save(p);
    }

    public List<Patient> findAll(){
        List<Patient> patients = patientRepository.findAll();

        return patients;
    }


    public Integer insert(Patient patient) {


        Patient p = patientRepository.save(patient);

        return p.getUserID();

    }

    public Integer update(Patient patient, int id) {

        Optional<Patient> pat = patientRepository.findById(id);
        // Patient d = new Patient();

        patient.setUserID(id);
        patientRepository.save(patient);
//        d.setPassword(doc.get().getPassword());
//        d.setUsername(doc.get().getUsername());
//        d.setRole(doc.get().getRole());
//        d.setName(doc.get().getName());
//        d.setEmail(doc.get().getEmail());
//        d.setPatientList(doc.get().getPatientList());
//        d.setCaregiverList(doc.get().getCaregiverList());
//        d.setIntakeList(doc.get().getIntakeList());
        //     patientRepository.save(d);

        return patient.getUserID();
    }

    public void delete(int id){
        this.patientRepository.deleteById(id);
    }
    
}
