package com.sd.assignment1.services;
//
//import com.sd.assignment1.dto.DoctorDTO;
//import com.sd.assignment1.dto.DoctorViewDTO;
//import com.sd.assignment1.dto.builders.DoctorBuilder;
//import com.sd.assignment1.dto.builders.DoctorViewBuilder;
import com.sd.assignment1.entities.Doctor;
import com.sd.assignment1.errorhandler.ResourceNotFoundException;
import com.sd.assignment1.repositories.DoctorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;


    public Doctor findDoctorById(Integer id){
        Optional<Doctor> doctor  = doctorRepository.findById(id);

        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "user id", id);
        }
        Doctor d = new Doctor();
        d.setUserID(doctor.get().getUserID());
        d.setPassword(doctor.get().getPassword());
        d.setUsername(doctor.get().getUsername());
        d.setRole(doctor.get().getRole());
        d.setName(doctor.get().getName());
        d.setEmail(doctor.get().getEmail());
        d.setPatientList(doctor.get().getPatientList());
        d.setCaregiverList(doctor.get().getCaregiverList());
        d.setIntakeList(doctor.get().getIntakeList());
        return doctorRepository.save(d);
    }

//    public Doctor findDoctorByUsername(String username){
//        List<Doctor> doctors = doctorRepository.findAll();
//        for(Doctor doctor: doctors){
//            if(doctor.getUsername().equals(username)) {
//                return doctor;
//            }
//        }
//        return null;
//    }
    public List<Doctor> findAll(){
        List<Doctor> doctors = doctorRepository.findAll();

        return doctors;
    }


    public Integer insert(Doctor doctor) {


        Doctor d = doctorRepository.save(doctor);

        return d.getUserID();

    }

    public Integer update(Doctor doctor, int id) {

        Optional<Doctor> doc = doctorRepository.findById(id);
       // Doctor d = new Doctor();

        doctor.setUserID(id);
        doctorRepository.save(doctor);
//        d.setPassword(doc.get().getPassword());
//        d.setUsername(doc.get().getUsername());
//        d.setRole(doc.get().getRole());
//        d.setName(doc.get().getName());
//        d.setEmail(doc.get().getEmail());
//        d.setPatientList(doc.get().getPatientList());
//        d.setCaregiverList(doc.get().getCaregiverList());
//        d.setIntakeList(doc.get().getIntakeList());
   //     doctorRepository.save(d);

        return doctor.getUserID();
    }

    public void delete(int id){
        this.doctorRepository.deleteById(id);
    }

}
