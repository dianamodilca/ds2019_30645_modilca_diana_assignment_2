package com.sd.assignment1.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "intake")
public class Intake {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idIntake")
    private Integer idIntake;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "end_date")
    private String endDate;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="patient_id")
    private Patient patient;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="doctor_id")
    private Doctor doctor;

    @ManyToMany(mappedBy="medicationIntakeList")
    Set<Medication> medicationList = new HashSet<Medication>();

    public Intake() {
    }

    public Intake(String startDate, String endDate, Patient patient, Doctor doctor, Set<Medication> medicationList) {
        this.startDate= startDate;
        this.endDate = endDate;
        this.patient = patient;
        this.doctor = doctor;
        this.medicationList = medicationList;
    }

    public Integer getIdIntake() {
        return idIntake;
    }

    public void setIdIntake(Integer idIntake) {
        this.idIntake = idIntake;
    }

    public Set<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(Set<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }
}
