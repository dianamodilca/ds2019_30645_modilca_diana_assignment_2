package com.sd.assignment1.entities;

import javax.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "doctor")
public class Doctor extends User implements Serializable {


    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;


    @OneToMany(fetch=FetchType.LAZY,  mappedBy = "doctor")
    private List<Patient> patientList = new ArrayList<>();

    @OneToMany(fetch=FetchType.LAZY,  mappedBy = "doctor")
    private List<Caregiver> caregiverList = new ArrayList<>();

    @OneToMany(fetch=FetchType.LAZY,  mappedBy = "doctor")
    private List<Intake> intakeList = new ArrayList<>();


    public Doctor() {
    }

    public Doctor(String name, String email, List<Patient> patientList, List<Caregiver> caregiverList, List<Intake> intakeList) {
        this.name = name;
        this.email = email;
        this.patientList = patientList;
        this.caregiverList = caregiverList;
        this.intakeList = intakeList;
    }

    public Doctor(Integer userID, String username, String password, String role, String name, String email, List<Patient> patientList, List<Caregiver> caregiverList, List<Intake> intakeList) {
        super(userID, username, password, role);
        this.name = name;
        this.email = email;
        this.patientList = patientList;
        this.caregiverList = caregiverList;
        this.intakeList = intakeList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<Patient> patientList) {
        this.patientList = patientList;
    }

    public List<Caregiver> getCaregiverList() {
        return caregiverList;
    }

    public void setCaregiverList(List<Caregiver> caregiverList) {
        this.caregiverList = caregiverList;
    }

    public List<Intake> getIntakeList() {
        return intakeList;
    }

    public void setIntakeList(List<Intake> intakeList) {
        this.intakeList = intakeList;
    }
}

