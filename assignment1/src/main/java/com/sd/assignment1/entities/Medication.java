package com.sd.assignment1.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
public class Medication{


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idMedication")
    private Integer idMedication;


    @Column(name = "name")
    private String name;

    @Column(name = "dosage")
    private Integer dosage;

    @JoinColumn(name = "patient_id")
    @ManyToOne
    private Patient patient;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "medication_intake",
            joinColumns = @JoinColumn(name = "idMedication"),
            inverseJoinColumns = @JoinColumn(name = "idIntake"))
    Set<Intake> medicationIntakeList;

    public Medication() {
    }

    public Medication(String name, Integer dosage, Patient patient, Set<Intake> medicationIntakeList) {
        this.name = name;
        this.dosage = dosage;
        this.patient = patient;
        this.medicationIntakeList = medicationIntakeList;
    }

    public Integer getIdMedication() {
        return idMedication;
    }

    public void setIdMedication(Integer idMedication) {
        this.idMedication = idMedication;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }


    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Set<Intake> getMedicationIntakeList() {
        return medicationIntakeList;
    }

    public void setMedicationIntakeList(Set<Intake> medicationIntakeList) {
        this.medicationIntakeList = medicationIntakeList;
    }
}



