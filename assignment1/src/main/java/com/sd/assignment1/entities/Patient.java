package com.sd.assignment1.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "patient")
public class Patient extends User implements Serializable {

    @Column(name="name")
    private String name;

    @Column(name="birth_date")
    private String birthDate;

    @Column(name="gender")
    private String gender;

    @Column(name="address")
    private String address;

    @Column(name = "medicalRecord")
    private String medicalRecord;


    @OneToMany(fetch=FetchType.LAZY,  mappedBy = "patient")
    private List<Intake> intakeList = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="doctor_id", nullable=true)
    private Doctor doctor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="caregiver_id", nullable=true)
    private Caregiver caregiver;

    public Patient() {
    }

    public Patient(String name, String birthDate, String gender, String address, String medicalRecord, List<Intake> intakeList, Doctor doctor, Caregiver caregiver) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.intakeList = intakeList;
        this.doctor = doctor;
        this.caregiver = caregiver;
    }

    public Patient(Integer userID, String username, String password, String role, String name, String birthDate, String gender, String address, String medicalRecord, List<Intake> intakeList, Doctor doctor, Caregiver caregiver) {
        super(userID, username, password, role);
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.intakeList = intakeList;
        this.doctor = doctor;
        this.caregiver = caregiver;
    }

//    public Caregiver getCaregiver() {
//        return caregiver;
//    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public List<Intake> getIntakeList() {
        return intakeList;
    }

    public void setIntakeList(List<Intake> intakeList) {
        this.intakeList = intakeList;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }
}

